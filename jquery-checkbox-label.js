/*!
 * Checkbox Labes jQuery Plugin
 *
 * @file: jquery-checkbox-label.js
 * @author: Tom Hnatovsky (@xom)
 * @site: xswd.cz
 * @license: MIT License
 */
;(function($) {
	$.fn.checkboxLabel = function(options){
		var elem = this;
		var settings = $.extend({
				class: 'selected'
		}, options );

		// Adds / removes label class name
		function markLabel(el, input){
			if (input.is(':checked')) {
				el.addClass(settings.class);
			}
			else {
				el.removeClass(settings.class);
			}
		};

		// Initialize
		elem.each(function(){
			var el = $(this);
			var input = $('input', el);
			markLabel(el, input);
			el.on('click', function(){
				markLabel(el, input);
			});
		});
		return this;
	};
}(jQuery));
