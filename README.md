jQuery Checkbox Label
=====================

Checkbox Label is a small jQuery plugin which turns labels around into a button which reacts on checkbox states. 

JS Integration
--------------

1. Link the latest jQuery in your HTML
```html
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
```

2. Link Checkbox Label plugin
```html
<script src="[your-script-location]/jquery-checkbox-label.js"></script>
```

3. Initialize the plugin
```javascript
$("label.checkbox-label").checkboxLabel();
```

HTML
----

Plugin counts on you have checkboxes wrapped in label tag.
```html
<label>
  <input name="checkbox" type="checkbox" value="1">
</label>
```

CSS Styling
-----------

Class name ```selected``` is added to labels of checked checkboxes by default. CSS is not provided in this plugin, styling is up to you.

I suggest using something like that:
```css
input[type=checkbox] {
  display: none;
}
label {
  color: #444;
  font-size: 12px;
  background: #ccc;
  border-radius: 5px;
  padding: 5px;
  cursor: pointer;  
}
label.selected {
  background: #777;
  color: #fff;
}
```


Options
-------

You can initialize the plugin with optional settings:

**class** (string)
This will overwrite default class name for labels with checked checkboxes.
```javascript
$("label.checkbox-label").checkboxLabel({class: 'checked'});
```